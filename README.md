# lsp_bot

Simple helper Scripts for www.leitstellenspiel.de
You need the package python3-pip. Use it to install the python3 libs mechanize, lxml and requests.

I only tested this script on Debian. It should run in theory on all other Linux Distros.
Python3 is platform independent, so in theory it should also run on Windows. But i don't tested this right now.

The project is licensed with Public Domain - feel free to contribute or to create a fork the project.


### Usage
Copy the `config.sample.json` to `config.json` and fill in your login data.  
After this, just run a script like e.g. `python3 hire_bot.py`.  

The scripts read the login data from the `config.json` file. Please make sure, that this file has a valid json syntax!  

Your login data is only used for the login at www.leitstellenspiel.de - the code is public, so just take a look if you're unsure.

### Disclaimer
Using this Scripts is against the rules of Leitstellenspiel. If you use it, you may get banned - so use it at your own risk.