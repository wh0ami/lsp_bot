#!/usr/bin/python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/lsp_bot

# Description:
# Just launch this script - it will do a login and set the hire days of all your departments to three days.

from mechanize import Browser
from lxml.html import fromstring
from time import *
import requests
import sys
import json
import os
from random import randint
from pathlib import Path


class Main:
    email = ''
    password = ''
    uid = ''
    site = ''
    auth_token = ''
    session = None
    header = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "de,en-US;q=0.7,en;q=0.3",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "DNT": "1",
        "Host": "www.leitstellenspiel.de",
        "Pragma": "no-cache",
        "TE": "Trailers",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0"
    }
    #"Content - Type": "application / x - www - form - urlencoded",

    buildings = {}
    typemap = {}



    def __init__(self):
        # read the login data from the configuration
        self.readConfig()

        # do a login
        self.login()

        # get a typemap for knowing, for mapping the building ids
        self.makeTypemap()

        # set hiring for all stations to three days
        self.hire()

        # regular end of the program
        sys.exit(0)


    def readConfig(self):
      # check whether a config exists
      try:
          configfile = os.path.dirname(os.path.realpath(__file__)) +"/config.json"
          # check whether the conig is a file
          if not Path(configfile).is_file():
              raise Excpetion("[Error] Could not read the config.json! Does it exist?")
      except:
          raise Exception("[Error] Config not found! Usage: ./hire_bot.py")

      # try to load the config
      try:
          with open(configfile, "r") as infile:
              config = json.load(infile)["config"]
              self.email = config["email"]
              self.password = config["password"]
      except:
          raise Exception("Error while opening the config file!")


    def makeTypemap(self) :
        # download the page for creating new buildings (because here are all informations, that we need)
        raw = self.session.get('https://www.leitstellenspiel.de/buildings/new')

        # extract the block with the buildings from the source code
        startpoint = raw.text.find('<select class="select required form-control" id="building_building_type" name="building[building_type]"><option value=""></option>') + 130
        endpoint = raw.text.find('</select>', startpoint)
        blocks = raw.text[startpoint:endpoint]

        # split the block in a list of sub blocks
        blocks = blocks.split('option>')

        for block in blocks:
            # Get the typeid of the building
            startpoint = block.find('<option value="') + 15
            endpoint = block.find('">')
            typeid = block[startpoint:endpoint]

            # Get the typedescription of the building
            startpoint = block.find('">') + 2
            endpoint = block.find('</')
            typedescription = block[startpoint:endpoint]

            # write data to dictionary
            self.typemap[typeid] = typedescription;


    def get_buildings(self):
        # download the standard page
        raw = self.session.get('https://www.leitstellenspiel.de/')

        # extract the javascript block for creating the map markers of the buildings
        startpoint = raw.text.find('buildingMarkerAdd')
        endpoint = raw.text.find('buildingMarkerBulkContentCacheDraw', startpoint)
        blocks = raw.text[startpoint:endpoint]

        # split the by paragraphs
        blocks = blocks.split('\n')

        # set the counter to zero
        i = 0

        for block in blocks:
            # Extract the Type of the building
            startpoint = block.find(',"building_type":') + 17
            endpoint = block.find(',"filter_id":')
            type = block[startpoint:endpoint]

            if type == "0" or type == "2" or type == "3" or type == "5" or type == "6" or type == "9" or type == "11" or type == "12" or type == "13" or type == "15" or type == "17" or type == "18" or type == "19" or type == "20":
                # Extract the ID of the building
                startpoint = block.find('"id":') + 5
                endpoint = block.find(',"user_id":')
                id = block[startpoint:endpoint]

                # Extract the name of the building
                startpoint = block.find(',"name":"') + 9
                endpoint = block.find('","longitude":')
                name = block[startpoint:endpoint]

                # Write the data to an dictionary
                self.buildings[i] = {}
                self.buildings[i]["id"] = str(id)
                self.buildings[i]["type"] = str(type)
                self.buildings[i]["name"] = str(name)
                i = i + 1


    def hire(self):
        # get a list of all buildings from the user
        self.get_buildings()

        # just send a simple GET request to https://www.leitstellenspiel.de/buildings/<building_id>/hire_do/3 for setting hiring to three days
        for (key, value) in self.buildings.items():
            x = self.session.get('https://www.leitstellenspiel.de/buildings/'+ str(value["id"]) +'/hire_do/3')
            print(strftime("%H:%M:%S") + ': Set hiring to three days for ['+ self.typemap[str(value["type"])] +'] '+ str(value["name"]) +' (ID '+ str(value["id"]) +')!')
            # sleep between two and seven seconds
            sleep(randint(2,7))


    def login(self):
        # prepare the data for a login
        url = "https://www.leitstellenspiel.de/users/sign_in"
        br = Browser()
        response = br.open(url)
        self.parse_token(response.read())

        data = {
            'authenticity_token': self.auth_token,
            'user[email]': self.email,
            'user[password]': self.password,
            'user[remember_me]': 1,
            'commit': 'Einloggen'
        }

        # do the login
        self.session = requests.session()
        self.session.headers.update(self.header)
        request = self.session.post(url, data=data)
        self.parse_token(request.text)

        # Extract the user id
        startpoint = request.text.find('var user_id = ') + 14
        endpoint = request.text.find(';', startpoint)
        self.uid = request.text[startpoint:endpoint]

        print(strftime("%H:%M:%S") + ': Successfully logged in as "'+ str(self.email) +'"! (User ID: '+ str(self.uid) +')')


    def parse_token(self, site):
        # Extract the authenticity token for logging in
        tree = fromstring(site)
        self.auth_token = tree.xpath('//meta[@name="csrf-token"]/@content')[0]


main = Main()
